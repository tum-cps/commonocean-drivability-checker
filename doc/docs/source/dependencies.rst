.. _dependencies:

System Requirements
-------------------

The software is written in Python 3.8 and tested on Linux. The usage of the Anaconda_ Python distribution is strongly recommended.

.. _Anaconda: http://www.anaconda.com/download/#download


Python packages (see also requirements file)

* `commonocean-io <https://gitlab.lrz.de/tum-cps/commonocean-io/>`_ (==2023.1)
* `commonocean-vessel-models <https://gitlab.lrz.de/tum-cps/commonocean-vessel-models/>`_ (==1.0.0)
* `commonocean-rules <https://gitlab.lrz.de/tum-cps/commonocean-rules/>`_ (==1.0.2)
* `commonroad-drivability-checker <https://gitlab.lrz.de/tum-cps/commonroad-drivability-checker>`_ (==2023.1)
* `numpy <https://pypi.org/project/numpy/>`_ (<=1.21.6)
* `Shapely <https://pypi.org/project/Shapely/>`_ (>=1.6.4)
* `matplotlib <https://pypi.org/project/matplotlib/>`_ (<=3.5.3)
* `Jupyter <https://pypi.org/project/jupyter/>`_ (>=1.0.0, for the tutorials)
* `Triangle <https://pypi.org/project/triangle/>`_ (>=20200424)
* `scipy <https://pypi.org/project/scipy/>`_ (<=1.7.2)
* `pandoc <https://pypi.org/project/pandoc/>`_ (>=1.0.2)
* `sphinx_rtd_theme <https://pypi.org/project/sphinx-rtd-theme/>`_ (>=0.4.3)
* `sphinx <https://pypi.org/project/Sphinx/>`_ (>=3.0.3)
* `nbsphinx_link <https://pypi.org/project/nbsphinx-link/>`_ (>=1.3.0)
* `nbsphinx <https://pypi.org/project/nbsphinx/>`_ (>=0.6.1)
* `breathe <https://pypi.org/project/breathe/>`_ (>=4.18.0)
* `polygon3 <https://pypi.org/project/Polygon/>`_ (>=3.0.8)

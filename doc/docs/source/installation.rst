.. _installation:

Installation
-----------------

All packages are available via `PyPi <https://pypi.org/>`_:

* `matplotlib <https://pypi.org/project/matplotlib/>`_
* `Shapely <https://pypi.org/project/Shapely/>`_
* `numpy <https://pypi.org/project/numpy/>`_
* `Jupyter <https://pypi.org/project/jupyter/>`_ 
* `Triangle <https://pypi.org/project/triangle/>`_ (Python bindings)
* `Scipy <https://pypi.org/project/scipy/>`_
* `Pandoc <https://pypi.org/project/pandoc/>`_
* `Sphinx_rtd_theme <https://pypi.org/project/sphinx-rtd-theme/>`_
* `Sphinx <https://pypi.org/project/Sphinx/>`_
* `nbspinxlink <https://pypi.org/project/nbsphinx-link/>`_
* `nbsphinx <https://pypi.org/project/nbsphinx/>`_
* `breathe <https://pypi.org/project/breathe/>`_
* `polygon3 <https://pypi.org/project/Polygon3/>`_
* `commonocean-io <https://gitlab.lrz.de/tum-cps/commonocean-io/>`_
* `commonocean-vessel-models <https://gitlab.lrz.de/tum-cps/commonocean-vessel-models/>`_
* `commonocean-rules <https://gitlab.lrz.de/tum-cps/commonocean-rules/>`_
* `commonroad-drivability-checker <https://gitlab.lrz.de/tum-cps/commonroad-drivability-checker>`_


They can be installed with the following command:

  .. code-block:: bash

      $ pip install -r requirements.txt


Installation of the CommonOcean Drivability Checker
**************************************************

After installing all essential packages, you can now install the CommonOcean Drivability Checker.

#. Open your console in the root folder of the CommonOcean Drivability Checker.

#. Activate your conda environment with

    .. code-block:: bash

            $ conda activate commonocean-py38
   
#. Install the CommonOcean Drivability Checker with

    .. code-block:: bash
        
            $ pip install -e .
    
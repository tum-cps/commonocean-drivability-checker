.. CommonOcean Drivability Checker documentation master file

CommonOcean Drivability Checker
==============================

Collision avoidance, kinematic feasibility, and waters-compliance must be validated to ensure the drivability of planned motions for autonomous vessels. These aforementioned checks are unified in the CommonOcean Drivability Checker that is compatible with the CommonOcean benchmark suite and can be used with hundreds of existing scenarios. 
The CommmonOcean Drivability Checker consists of five core modules:

- **collision**: The collision checker module checks whether given geometric objects (e.g., rectangles or triangles) collide with each other. Based on the geometric representation, we provide a computationally efficient method to represent complex traffic scenarios for collision checking.

- **boundary**: The waters boundary module determines the road compliance of a given trajectory by either checking whether the ego vessel is still fully enclosed in the waters network or collision checking with obstacles that model the boundary of the waters network. Our module provides two different approaches (triangulation and the creation of oriented rectangles) to generate waters boundary obstacles.

- **geometry**: The geometry module creates a curvilinear coordinate system aligned to the given reference path. The unique projection domain along the reference path is automatically computed.

- **feasibility**: The feasibility module builds on top of the vehicle models provided by CommonOcean. It determines the feasibility of a given trajectory by reconstructing the input to the corresponding (non-linear) vehicle model. Trajectories are feasible if the obtained input respects the constraints of the vehicle model, e.g., limited yaw rate.

- **costs**: The costs module implements cost functions of the CommonOcean Benchmark to evaluate solutions designed by the user.

Getting Started 
------------

Head over to :ref:`Getting Started<gettingStarted>` to learn how to obtain and install the CommonOcean Drivability Checker.
In :ref:`Tutorials <overview>`, you can find jupyter notebooks on how to use the CommonOcean drivability checker.

Overview
------------

.. toctree::
   :maxdepth: 2
    
    Getting Started <gettingStarted.rst>
    Tutorials <overview.rst>
    API <api.rst>

Contact information
------------

:Website: `commonocean.cps.cit.tum.de <https://commonocean.cps.cit.tum.de>`_
:Email: `commonocean@lists.lrz.de <commonocean@lists.lrz.de>`_

.. _api_feasibility:

Module feasibility
==================

feasibility_checker
-------------------

.. automodule:: commonocean_dc.feasibility.feasibility_checker
   :members:
   :undoc-members:
   :member-order: bysource

solution_checker
----------------

.. automodule:: commonocean_dc.feasibility.solution_checker
   :members:
   :undoc-members:
   :member-order: bysource

vessel_dynamics
----------------

.. automodule:: commonocean_dc.feasibility.vessel_dynamics
   :members:
   :undoc-members:
   :member-order: bysource

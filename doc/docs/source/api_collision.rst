.. _api_collision:

Module collision
================

collision_detection
-------------------

.. automodule:: commonocean_dc.collision.collision_detection.pycrcc_collision_dispatch
   :members:
   :undoc-members:
   :member-order: bysource

trajectory_queries
-------------------

.. automodule:: commonocean_dc.collision.trajectory_queries.trajectory_queries
   :members:
   :undoc-members:
   :member-order: bysource

visualization
-------------

.. automodule:: commonocean_dc.collision.visualization.draw_dispatch
   :members:
   :undoc-members:
   :member-order: bysource


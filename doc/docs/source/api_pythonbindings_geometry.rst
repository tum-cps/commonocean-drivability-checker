.. _api_pythonbindings_geometry:

Python Bindings pycrccosy
======================

.. automodule:: commonocean_dc.pycrccosy

``Segment`` class
^^^^^^^^^^^^^^^
.. autoclass:: Segment
   :members:
   :special-members:

``CurvilinearCoordinateSystem`` class
^^^^^^^^^^^^^^^^^^
.. autoclass:: CurvilinearCoordinateSystem
   :members:
   :special-members:

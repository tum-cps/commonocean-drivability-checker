# Changelog

## [2023.1] - 2023-09-28

### Added

- New auxiliary function to check the drivability with respect to the depth of shallows/waters 

### Changed

- Improved constructing static obstacles representing collidable waters boundary from the Navigationable Area specified in the XML scenarios
- The installation of third-party libraries is not necessary anymore, due to a refactoring in the module
- The module now uses the new version of CommonOcean IO (2023.1)
- The package is no longer compatible with Python 3.7
